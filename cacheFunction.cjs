function cacheFunction(cb) {
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
    if(arguments.length !== 1 || typeof cb !== 'function') {
        throw new Error('Invalid');
    }
    let cacheObj = {}
    const insideFunc = (...args) => {
        // console.log(cacheObj)
        if(cacheObj[[...args]] ){
            return cacheObj[[...args]]
        }
        cacheObj[[...args]] = cb(...args)
        return cacheObj[[...args]] 
    }
    return insideFunc
}

module.exports = cacheFunction