function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is! acceptable if cb can't be returned
    if(arguments.length !== 2 || typeof cb !== 'function' || typeof n !== 'number' ) {
        throw new Error('Invalid');
    }
    let counter = n
    const insideFunc = (...args) => {
        if (counter <= 0) {
            return null
        } else {
            counter -= 1
        }
        return cb(...args)
    }
    return insideFunc


}

module.exports = limitFunctionCallCount