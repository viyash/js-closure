let cacheFunction = require('../cacheFunction.cjs')

try {
    let ac = cacheFunction((a, b) => { return a, b })
    console.log(ac(1, 2, 3))
    console.log(ac(1, 2))
    console.log(ac(1, 2, 3))
} catch (e) {
    console.log(e)
}
