let limitFunctionCallCount = require('../limitFunctionCallCount.cjs')

let a = limitFunctionCallCount((a) => {return a;}, 1);

try {
    console.log(a([1,2,3]))
    console.log(a(1,2,2))
    console.log(a(1,2,2))
    console.log(a(1,2,2)) 
} catch (error) {
    console.log(error);
}
